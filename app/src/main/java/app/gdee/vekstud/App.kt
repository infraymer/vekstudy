package app.gdee.vekstud

import android.app.Application

import com.chibatching.kotpref.Kotpref

import io.realm.Realm
import io.realm.RealmConfiguration


/**
 * Created by G_Dee on 25.01.2017.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initRealm()
        Kotpref.init(this)
    }

    private fun initRealm() {
        Realm.init(this)
        val config = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(config)
    }
}
