package app.gdee.vekstud

/**
 * Created by infraymer on 01.09.17.
 */
object ScheduleManager {

    /***********************************************************************************************
     * Interfaces
     **********************************************************************************************/
    interface ChoiceFinishCallback {
        fun onFinish(done: Boolean)
    }

    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    lateinit var mChoiceFinishCallback: ChoiceFinishCallback

    /***********************************************************************************************
     * Setters interfaces
     **********************************************************************************************/
    fun setChoiceFinishCallback(l: (done: Boolean) -> Unit) {
        mChoiceFinishCallback = object : ChoiceFinishCallback {
            override fun onFinish(done: Boolean) {
                l(done)
            }
        }
    }
}