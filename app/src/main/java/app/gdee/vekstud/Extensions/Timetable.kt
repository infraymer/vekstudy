@file:Suppress("NOTHING_TO_INLINE")

package app.gdee.vekstud.Extensions

import app.gdee.vekstud.Realm.GroupR
import app.gdee.vekstud.Realm.LectorLocationR
import app.gdee.vekstud.Realm.LessonR
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable
import io.realm.RealmList

/**
 * Created by infraymer on 01.09.17.
 */
inline fun DataTimetable.convertToRealmLessons(): RealmList<LessonR> {
    val map = timetable
    val lessons = RealmList<LessonR>()
    for (item in map) {
        for (lesson in item.value) {
            val les = LessonR()
            les.date = lesson.date
            les.timeBegin = lesson.timeBegin
            les.timeEnd = lesson.timeEnd
            les.subjectName = lesson.subject
            les.location = lesson.location
            if (lesson.group != null) {
                les.groupId = lesson.group.id
                les.groupName = lesson?.group?.name!!
                if (lesson.group.subgroup != null) {
                    les.subgroupId = lesson.group.subgroup?.id!!
                    les.subgroupName = lesson.group.subgroup?.name!!
                }
            }
            if (lesson.lector != null) {
                les.lectorId = lesson.lector?.id!!
                les.lectorName = lesson.lector?.name!!
            }
            les.typeId = lesson.lessonType.id
            les.typeName = lesson.lessonType.name
            if (lesson.groups != null) {
                for (group in lesson.groups) {
                    val gr = GroupR()
                    gr.id = group.id
                    gr.name = group?.name!!
                    if (group.subgroup != null)
                        gr.subgroup = group.subgroup?.name!!
                    les.groups.add(gr)
                }
            }
            if (lesson.lectorLocations != null) {
                for (lecLoc in lesson.lectorLocations) {
                    val ll = LectorLocationR()
                    ll.lector = lecLoc.lector
                    ll.location = lecLoc.location
                    les.lectorLocations.add(ll)
                }
            }
            lessons.add(les)
        }
    }
    return lessons
}