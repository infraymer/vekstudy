@file:Suppress("NOTHING_TO_INLINE")

package app.gdee.vekstud.Extensions

import android.view.View

/**
 * Created by infraymer on 01.09.17.
 */
inline fun View.visible() {
    visibility = View.VISIBLE
}

inline fun View.invisible() {
    visibility = View.INVISIBLE
}

inline fun View.gone() {
    visibility = View.GONE
}