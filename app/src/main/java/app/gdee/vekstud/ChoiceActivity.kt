package app.gdee.vekstud

import android.os.Bundle
import app.gdee.vekstud.Adapters.PrimitiveAdapter
import app.gdee.vekstud.Extensions.gone
import app.gdee.vekstud.Extensions.visible
import app.gdee.vekstud.Presenters.ChoicePresenter
import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.VekAPI.JSON.Objects.University
import app.gdee.vekstud.Views.ChoiceView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arlib.floatingsearchview.FloatingSearchView
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import kotlinx.android.synthetic.main.activity_choice.*
import kotlinx.android.synthetic.main.layout_error.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast

/**
 * Created by infraymer on 03.09.17.
 */
class ChoiceActivity : MvpAppCompatActivity(), ChoiceView {

    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    @InjectPresenter
    lateinit var mPresenter: ChoicePresenter
    private lateinit var mPrimitiveAdapter: PrimitiveAdapter

    /***********************************************************************************************
     * Override methods
     **********************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choice)
        initToolbar()
        initSearchView()
        initListView()
        initError()
    }

    /***********************************************************************************************
     * Private methods
     **********************************************************************************************/
    private fun initToolbar() {
        choice_toolbar.setTitle(R.string.choice_univer)
        choice_toolbar.navigationIconResource = R.drawable.ic_arrow_back_white_24dp
        choice_toolbar.setNavigationOnClickListener { mPresenter.onBackPressed() }
    }

    private fun initSearchView() {
        choice_search_view.gone()
        choice_search_view.setOnHomeActionClickListener { mPresenter.onHomeActionSearchClicked() }
        choice_search_view.setOnQueryChangeListener { oldQuery, newQuery ->
            mPresenter.onQueryChanged(newQuery)
        }
        choice_search_view.setOnSearchListener(object : FloatingSearchView.OnSearchListener {
            override fun onSearchAction(currentQuery: String?) {
                if (currentQuery != null)
                    mPresenter.onSuggestionItemClicked(currentQuery)
            }

            override fun onSuggestionClicked(searchSuggestion: SearchSuggestion?) {
                if (searchSuggestion != null)
                    mPresenter.onSuggestionItemClicked(searchSuggestion.body)
            }

        })
    }

    private fun initError() {
        error_button_update.onClick { mPresenter.onUpdateButtonClicked() }
    }

    private fun initListView() {
        choice_list.setOnItemClickListener { parent, view, position, id ->
            mPresenter.onUniverClicked(mPrimitiveAdapter.getItem(position) as University)
        }
    }

    /***********************************************************************************************
     * Choice View
     **********************************************************************************************/
    override fun showError(resId: Int) {
        error_textView.text = getString(resId)
        error_content.visible()
    }

    override fun hideError() {
        error_content.gone()
    }

    override fun showListUniversity(universities: ArrayList<University>) {
        mPrimitiveAdapter = PrimitiveAdapter(this, universities)
        choice_list.adapter = mPrimitiveAdapter
    }

    override fun setSearchEntityList(entity: ArrayList<Entity>) {
        choice_search_view.swapSuggestions(entity)
    }

    override fun showProgress() {
        choice_progress.visible()
    }

    override fun hideProgress() {
        choice_progress.gone()
    }

    override fun showSearch() {
        choice_search_view.visible()
        choice_search_view.setSearchFocused(true)
    }

    override fun hideSearch() {
        choice_search_view.gone()
    }

    override fun showMessage(resId: Int) {
        toast(resId)
    }

    override fun showSearchProgress() {
        choice_search_view.showProgress()
    }

    override fun hideSearchProgress() {
        choice_search_view.hideProgress()
    }

    override fun finishActivity() {
        finish()
    }
}