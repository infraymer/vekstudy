package app.gdee.vekstud.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.gdee.vekstud.Helpers.Utils
import app.gdee.vekstud.R
import app.gdee.vekstud.Realm.CacheRoleR
import app.gdee.vekstud.Realm.LessonR
import io.realm.Realm
import io.realm.RealmResults
import khronos.Dates
import khronos.day
import khronos.plus
import khronos.toString
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

/**
 * Created by G_Dee on 15.02.2017.
 */

class BrowseDayFragment(
    private val mPosition: Int,
    private val mRoleId: Int,
    private val mRoleName: String,
    private val mIsStudent: Boolean
) : Fragment() {


    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private val mRealm = Realm.getDefaultInstance()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // val rootView = inflater!!.inflate(R.layout.fragment_timetable, container, false)

        val date = Dates.today.plus(mPosition.day).toString("yyyy-MM-dd")

        val role = mRealm.where(CacheRoleR::class.java)
            .equalTo("id", mRoleId)
            .findFirst()

        val lessons = role.lessons.where().equalTo("date", date).findAll()

        if (lessons.isEmpty())
            return lessonsIsEmptyView()

        when (mIsStudent) {
            true -> return groupItemView(lessons)
            false -> return lectorItemView(lessons)
        }
    }


    fun groupItemView(lessons: RealmResults<LessonR>?): View {
        val ID_TIME_BEGIN = 1
        val ID_TIME_END = 2
        val ID_LINE = 3
        val ID_TYPE = 4
        val ID_SUBGROUP = 5
        val ID_LIST = 6
        val ID_LIST_BUTTON = 7


        return UI {
            verticalLayout {
                scrollView {
                    verticalLayout {
                        if (lessons != null && lessons.isNotEmpty()) {
                            for (lesson in lessons) {
                                // Слой элемента списка занятий
                                view {
                                    backgroundColor = 0xCCCCCC.opaque
                                }.lparams(matchParent, dip(1))
                                verticalLayout {
                                    lparams {
                                        width = matchParent
                                        leftMargin = dip(16)
                                        rightMargin = dip(16)
                                        topMargin = dip(8)
                                        bottomMargin = dip(8)
                                    }
                                    // Верхний слой
                                    relativeLayout() {
                                        lparams(matchParent)
                                        // Время начала
                                        textView(lesson.timeBegin) {
                                            id = ID_TIME_BEGIN
                                            backgroundColor = resources.getColor(R.color.colorAccent)
                                            textColor = resources.getColor(R.color.white)
                                            setPadding(dip(4), 0, dip(4), 0)
                                        }
                                        // Время конца
                                        textView(" - ${lesson.timeEnd}") {
                                            id = ID_TIME_END
                                            textColor = resources.getColor(R.color.colorAccent)
                                        }.lparams {
                                            rightOf(ID_TIME_BEGIN)
                                            leftMargin = dip(4)
                                        }
                                        // Тип занятия
                                        cardView {
                                            cardElevation = dip(2).toFloat()
                                            id = ID_TYPE
                                            linearLayout {
                                                textView(lesson.typeName.toLowerCase()) {
                                                    backgroundColor = Utils.getColorType(lesson.typeId)
                                                    textColor = resources.getColor(R.color.white)
                                                    setPadding(dip(4), 0, dip(4), 0)
                                                }
                                                // Подгруппа
                                                if (lesson.subgroupId != 0)
                                                    textView("  ${lesson.subgroupName}  ") {
                                                        id = ID_SUBGROUP
                                                        backgroundColor = resources.getColor(R.color.colorAccent)
                                                        textColor = resources.getColor(R.color.white)
                                                    }.lparams {
                                                    }
                                            }
                                        }.lparams {
                                            alignParentRight()
                                            bottomMargin = dip(4)
                                            rightMargin = dip(2)
                                        }

                                    }
                                    // Дисциплина
                                    textView(lesson.subjectName) {
                                        textColor = resources.getColor(R.color.black)
                                        textSize = 16f
                                    }
                                    relativeLayout() {
                                        verticalLayout() {
                                            id = ID_LIST
                                            // Слой препода с иконкой
                                            linearLayout {
                                                view {
                                                    backgroundDrawable = resources.getDrawable(R.drawable.ic_teacher_grey_vector)
                                                }.lparams(dip(14), dip(14)) {
                                                    gravity = Gravity.CENTER_VERTICAL
                                                }
                                                textView(lesson.lectorLocations[0].lector) {
                                                    textSize = 13f
                                                }.lparams {
                                                    leftMargin = dip(4)
                                                    gravity = Gravity.CENTER_VERTICAL
                                                }
                                            }
                                            // Слой аудитории с иконкой
                                            linearLayout {
                                                view {
                                                    backgroundDrawable = resources.getDrawable(R.drawable.ic_location_vector)
                                                }.lparams(dip(14), dip(14)) {
                                                    gravity = Gravity.CENTER_VERTICAL
                                                }
                                                textView(lesson.lectorLocations[0].location) {
                                                    textSize = 13f
                                                }.lparams {
                                                    leftMargin = dip(4)
                                                    gravity = Gravity.CENTER_VERTICAL
                                                }
                                            }
                                        }
                                        // Слой со списком препод - аудитория
                                        val vg = verticalLayout {

                                            // Цикл с отображением списка препод - аудитория
                                            for (item in lesson.lectorLocations) {
                                                if (item.lector == lesson.lectorLocations[0].lector)
                                                    continue
                                                // Слой препода с иконкой
                                                linearLayout {
                                                    view {
                                                        backgroundDrawable = resources.getDrawable(R.drawable.ic_teacher_grey_vector)
                                                    }.lparams(dip(14), dip(14)) {
                                                        gravity = Gravity.CENTER_VERTICAL
                                                    }
                                                    textView(item.lector) {
                                                        textSize = 13f
                                                    }.lparams {
                                                        leftMargin = dip(4)
                                                        gravity = Gravity.CENTER_VERTICAL
                                                    }
                                                }
                                                // Слой аудитории с иконкой
                                                linearLayout {
                                                    view {
                                                        backgroundDrawable = resources.getDrawable(R.drawable.ic_location_vector)
                                                    }.lparams(dip(14), dip(14)) {
                                                        gravity = Gravity.CENTER_VERTICAL
                                                    }
                                                    textView(item.location) {
                                                        textSize = 13f
                                                    }.lparams {
                                                        leftMargin = dip(4)
                                                        gravity = Gravity.CENTER_VERTICAL
                                                    }
                                                }
                                            }
                                        }.lparams {
                                            below(ID_LIST)
                                        }
                                        vg.visibility = View.GONE

                                        val ib = imageView(R.drawable.ic_open_grey_18_vector) {
                                            onClick {
                                                when (vg.isShown) {
                                                    true -> {
                                                        vg.visibility = View.GONE
                                                        imageResource = R.drawable.ic_open_grey_18_vector
                                                    }
                                                    false -> {
                                                        vg.visibility = View.VISIBLE
                                                        imageResource = R.drawable.ic_close_grey_18_vector
                                                    }

                                                }
                                            }
                                        }.lparams(dip(18), dip(18)) {
                                            rightOf(ID_LIST)
                                            leftMargin = dip(16)
                                            topMargin = dip(1)
                                        }
                                        if (lesson.lectorLocations.size <= 2)
                                            ib.visibility = View.GONE
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }.view
    }

    fun lectorItemView(lessons: RealmResults<LessonR>?): View {
        val ID_TIME_BEGIN = 1
        val ID_TIME_END = 2
        val ID_LINE = 3
        val ID_TYPE = 4
        val ID_SUBGROUP = 5

        return UI {
            verticalLayout {
                scrollView {
                    verticalLayout {
                        if (lessons != null && lessons.isNotEmpty()) {
                            for (lesson in lessons) {
                                // Слой элемента списка занятий
                                view {
                                    backgroundColor = 0xCCCCCC.opaque
                                }.lparams(matchParent, dip(1))
                                verticalLayout {
                                    lparams {
                                        width = matchParent
                                        leftMargin = dip(16)
                                        rightMargin = dip(16)
                                        topMargin = dip(8)
                                        bottomMargin = dip(8)
                                    }
                                    // Верхний слой
                                    relativeLayout() {
                                        lparams(matchParent)
                                        // Время начала
                                        textView(lesson.timeBegin) {
                                            id = ID_TIME_BEGIN
                                            backgroundColor = resources.getColor(R.color.colorAccent)
                                            textColor = resources.getColor(R.color.white)
                                            setPadding(dip(4), 0, dip(4), 0)
                                        }
                                        // Время конца
                                        textView(" - ${lesson.timeEnd}") {
                                            id = ID_TIME_END
                                            textColor = resources.getColor(R.color.colorAccent)
                                        }.lparams {
                                            rightOf(ID_TIME_BEGIN)
                                            leftMargin = dip(4)
                                        }
                                        // Тип занятия
                                        cardView {
                                            cardElevation = dip(2).toFloat()
                                            textView(lesson.typeName.toLowerCase()) {
                                                id = ID_TYPE
                                                backgroundColor = Utils.getColorType(lesson.typeId)
                                                textColor = resources.getColor(R.color.white)
                                                setPadding(dip(4), 0, dip(4), 0)
                                            }
                                        }.lparams {
                                            alignParentRight()
                                            bottomMargin = dip(4)
                                            rightMargin = dip(2)
                                        }
                                    }
                                    // Дисциплина
                                    textView(lesson.subjectName) {
                                        textColor = resources.getColor(R.color.black)
                                        textSize = 16f
                                    }
                                    // Цикл с отображением списка препод - аудитория
                                    for (group in lesson.groups) {
                                        // Слой препода с иконкой
                                        linearLayout {
                                            view {
                                                backgroundDrawable = resources.getDrawable(R.drawable.ic_group_grey_vector)
                                            }.lparams(dip(14), dip(14)) {
                                                gravity = Gravity.CENTER_VERTICAL
                                            }
                                            val sub = if (group.subgroup != "") " / Подгруппа ${group.subgroup}" else ""
                                            textView("${group.name} $sub") {
                                                textSize = 13f
                                            }.lparams {
                                                leftMargin = dip(4)
                                                gravity = Gravity.CENTER_VERTICAL
                                            }
                                        }
                                    }
                                    // Слой аудитории с иконкой
                                    linearLayout {
                                        view {
                                            backgroundDrawable = resources.getDrawable(R.drawable.ic_location_vector)
                                        }.lparams(dip(14), dip(14)) {
                                            gravity = Gravity.CENTER_VERTICAL
                                        }
                                        textView(lesson.location) {
                                            textSize = 13f
                                        }.lparams {
                                            leftMargin = dip(4)
                                            gravity = Gravity.CENTER_VERTICAL
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }.view
    }

    fun lessonsIsEmptyView(): View {
        val ID_TEXT = 1

        return UI {
            relativeLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                }
                imageView(R.drawable.smile_grey_vector) {
                }.lparams {
                    centerInParent()
                    above(ID_TEXT)
                    bottomMargin = dip(16)
                }
                textView(R.string.no_lessons) {
                    id = ID_TEXT
                    textSize = 16f
                }.lparams {
                    centerInParent()
                }
            }
        }.view
    }
}