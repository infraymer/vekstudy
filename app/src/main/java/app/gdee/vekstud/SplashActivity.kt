package app.gdee.vekstud

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import org.jetbrains.anko.startActivity

/**
 * Created by infraymer on 29.04.17.
 */

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity<MainActivity>()
        finish()
    }
}
