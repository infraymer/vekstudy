package app.gdee.vekstud.Realm

import io.realm.RealmObject

/**
 * Created by infraymer on 27.05.17.
 */
open class LectorLocationR : RealmObject() {
    var lector: String = ""
    var location: String = ""
}