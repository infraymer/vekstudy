package app.gdee.vekstud.Realm

import io.realm.RealmList
import io.realm.RealmObject

/**
 * Created by infraymer on 27.05.17.
 */
open class LessonR : RealmObject() {
    var date: String = ""
    var timeBegin: String = ""
    var timeEnd: String = ""
    var subjectName: String = ""
    var typeName: String = ""
    var typeId: Int = 0
    var lectorName: String = ""
    var lectorId: Int = 0
    var groups: RealmList<GroupR> = RealmList()
    var groupName: String = ""
    var groupId: Int = 0
    var subgroupName: String = ""
    var subgroupId: Int = 0
    var lectorLocations: RealmList<LectorLocationR> = RealmList()
    var location: String = ""
}