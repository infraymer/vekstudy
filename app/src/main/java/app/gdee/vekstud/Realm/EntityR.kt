package app.gdee.vekstud.Realm

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by infraymer on 27.05.17.
 */
open class EntityR : RealmObject() {
    @PrimaryKey
    var PK = ""
    var id: Int = 0
    var name: String = ""
    var isGroup: Boolean = false
    var lessons: RealmList<LessonR> = RealmList()
}