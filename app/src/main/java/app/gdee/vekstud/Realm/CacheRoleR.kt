package app.gdee.vekstud.Realm

import io.realm.RealmList
import io.realm.RealmObject

/**
 * Created by infraymer on 27.05.17.
 */
open class CacheRoleR : RealmObject() {
    var id: Int = 0
    var name: String = ""
    var isStudent: Boolean = false
    var lessons: RealmList<LessonR> = RealmList()
}