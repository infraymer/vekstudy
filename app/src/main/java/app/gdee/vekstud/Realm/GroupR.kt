package app.gdee.vekstud.Realm

import io.realm.RealmObject

/**
 * Created by infraymer on 27.05.17.
 */
open class GroupR : RealmObject() {

    var id: Int = 0
    var name: String = ""
    var subgroup: String = ""

}