package app.gdee.vekstud

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import app.gdee.vekstud.Adapters.Animation.ZoomOutPageTransformer
import app.gdee.vekstud.Adapters.ViewPagerAdapter
import app.gdee.vekstud.Extensions.gone
import app.gdee.vekstud.Extensions.invisible
import app.gdee.vekstud.Extensions.visible
import app.gdee.vekstud.Presenters.DrawerPresenter
import app.gdee.vekstud.Presenters.MainPresenter
import app.gdee.vekstud.Presenters.SearchPresenter
import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.Views.DrawerView
import app.gdee.vekstud.Views.MainView
import app.gdee.vekstud.Views.SearchView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_timetable.*
import kotlinx.android.synthetic.main.app_bar_timetable.*
import kotlinx.android.synthetic.main.content_timetable.*
import kotlinx.android.synthetic.main.layout_error.*
import org.jetbrains.anko.share
import org.jetbrains.anko.startActivity

/**
 * Created by infraymer on 17.05.17.
 */

class MainActivity : MvpAppCompatActivity(), MainView, SearchView, DrawerView, NavigationView.OnNavigationItemSelectedListener {


    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    @InjectPresenter
    lateinit var mMainPresenter: MainPresenter
    @InjectPresenter
    lateinit var mSearchPresenter: SearchPresenter
    @InjectPresenter
    lateinit var mDrawerPresenter: DrawerPresenter

    private lateinit var mViewPagerAdapter: ViewPagerAdapter

    /***********************************************************************************************
     * Override funs
     **********************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timetable)
        error_button_update.setOnClickListener { mMainPresenter.onUpdateButtonClicked() }
//        timetable_toolbar.titleMarginStart = 52
        initSearch()
        initNavigationDrawer()
//        initViewPagerAdapter()
        mMainPresenter.onCreated()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    /***********************************************************************************************
     * Private funs
     **********************************************************************************************/
    // Инициализация бокового меню
    private fun initNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, timetable_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        drawer_layout.setDrawerListener(toggle)
        toggle.syncState()

        nav_view.background = resources.getDrawable(R.drawable.back_vector)
        nav_view.setNavigationItemSelectedListener(this)
    }

    // Инициализация поиска
    private fun initSearch() {
        timetable_search_view.setOnHomeActionClickListener { mSearchPresenter.onHomeButtonClicked() }
    }

    private fun initViewPagerAdapter() {
        mViewPagerAdapter = ViewPagerAdapter(supportFragmentManager, this)
        timetable_container.adapter = mViewPagerAdapter
        timetable_container.setPageTransformer(true, ZoomOutPageTransformer())
        timetable_tabs.setViewPager(timetable_container)
        timetable_tabs.visible()
    }

    /***********************************************************************************************
     * SearchView
     **********************************************************************************************/
    override fun showSearch() {
        timetable_search_view.visible()
    }

    override fun hideSearch() {
        timetable_search_view.invisible()
    }

    override fun setSearchList() {
        // тут надо пихать данные в список поиска
    }

    override fun showSearchProgress() {

    }

    override fun hideSearchProgress() {
    }

    override fun setSearchEntityList(entity: ArrayList<Entity>) {
    }

    /***********************************************************************************************
     * MainView
     **********************************************************************************************/
    // Показать ошибку
    override fun showError() {
        timetable_tabs.gone()
        error_content.visible()
    }

    // Скрыть ошибку
    override fun hideError() {
        timetable_tabs.visible()
        error_content.gone()
    }

    // Показать прогресс
    override fun showProgress() {
        timetable_tabs.gone()
        timetable_progress.visible()
    }

    // Скрыть прогресс
    override fun hideProgress() {
        timetable_tabs.visible()
        timetable_progress.gone()
    }

    // Установить заголовок в экшен баре
    override fun setTitle(value: String) {
        timetable_toolbar.title = value
    }

    override fun updateTimetable() {
        initViewPagerAdapter()
    }

    override fun startChoiceActivity() {
        startActivity<ChoiceActivity>()
    }

    /***********************************************************************************************
     * DrawerView
     **********************************************************************************************/
    override fun startShareVia() {
        share("Теперь расписание всегда с тобой. https://play.google.com/store/apps/details?id=app.gdee.vekstud")
    }

    override fun startChoiceEntity() { }

    override fun startAboutApp() {
        startActivity<AboutActivity>()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        /*alert(R.string.do_you_want_reset_settings) {
            yesButton {
                it.dismiss()
                onNavigationItemClick(R.id.nav_settings)
            }
            noButton { it.cancel() }
        }*/
        mDrawerPresenter.onMenuItemClicked(item.itemId)
        this.drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
