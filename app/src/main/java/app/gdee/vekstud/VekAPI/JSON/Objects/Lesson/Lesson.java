package app.gdee.vekstud.VekAPI.JSON.Objects.Lesson;

import android.graphics.Color;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.gdee.vekstud.Adapters.LectorLocation;
import app.gdee.vekstud.Helpers.Utils;

/**
 * Created by G_Dee on 19.01.2017.
 */

public class Lesson {

    private static final int YEAR = 0;
    private static final int MONTH = 1;
    private static final int DAY = 2;

    private static final int HOUR = 0;
    private static final int MINUTE = 1;
    private static final int SECOND = 2;

    private static final String LK = "Лек";
    public static final String LECTURE = "Лекция";
    private static final String LB = "Лб";
    public static final String LAB = "Лаба";
    private static final String PR = "Пр";
    public static final String PRACTICE = "Практика";

    private static final String COLOR_LK = "#3cc238";
    private static final String COLOR_PR = "#ff9600";
    private static final String COLOR_LB = "#ff6600";

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("time_begin")
    @Expose
    private String timeBegin;

    @SerializedName("time_end")
    @Expose
    private String timeEnd;

    @SerializedName("subject_name")
    @Expose
    private String subject;

    @SerializedName("lesson_type")
    @Expose
    private LessonType lessonType;

    @SerializedName("lector")
    @Expose
    private LectorLesson lector;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("group")
    @Expose
    private GroupLesson group;

    @SerializedName("groups")
    @Expose
    private ArrayList<GroupLesson> groups;


    private ArrayList<LectorLocation> lectorLocations = new ArrayList<>();

    public void clearLectorLocations() {
        lectorLocations.remove(0);
    }

    public void fillLectorLocations() {
        String lector = getLector().getName();
        String loc = getLocation();
        lectorLocations.add(0, new LectorLocation(lector, location));
    }

    // Добавление в список преподавателя и кабинет
    // это нужно для отображения расписания
    public void addLectorLocation(LectorLocation lectorLocation) {
        if (lectorLocations == null)
            this.lectorLocations = new ArrayList<>();


        lectorLocations.add(lectorLocation);
    }

    public void addLectorLocation(LectorLocation lectorLocation, int position) {
        if (lectorLocations == null)
            this.lectorLocations = new ArrayList<>();

        lectorLocations.add(position, lectorLocation);
    }

    // Возвращает наименование групп
    public ArrayList<String> getNamesGroups() {
        ArrayList<String> names = new ArrayList<>();
        for (GroupLesson group : groups) {
            names.add(group.getName());
        }

        return names;
    }

    // Разделить дату по частям
    private String getSplitDate(int part) {
        return this.date.split("-")[part];
    }

    // Получить день
    public int getDay() {
        return Integer.parseInt(getSplitDate(DAY));
    }

    // Получиьть месяц
    public int getMonth() {
        return Integer.parseInt(getSplitDate(MONTH));
    }

    // Получить год
    public int getYear() {
        return Integer.parseInt(getSplitDate(YEAR));
    }

    // Получить месяц
    public String getMonthName() {
        return Utils.getMonthName(this.date);
    }

    // Получить день недели
    public String getDayOfWeekName() {
        return Utils.getDayOfWeekName(this.date);
    }

    public int getTypeColor() {
        switch (lessonType.getId()) {
            case 1:
                return Color.parseColor("#4e95f4");
            case 2:
                return Color.parseColor("#2da9bf");
            case 3:
                return Color.parseColor("#77cb1c");
            case 4:
                return Color.parseColor("#815bde");
            case 5:
                return Color.parseColor("#ee4364");
            case 6:
                return Color.parseColor("#fb9500");
            default:
                return Color.parseColor("#915437");
        }
    }

    /***********************************************************************************************
     * ГЕТТЕРЫ
     **********************************************************************************************/
    public ArrayList<GroupLesson> getGroups() {
        return groups;
    }

    public String getDate() {
        return date;
    }

    public String getTimeBegin() {
        String[] time = this.timeBegin.split(":");
        return time[HOUR] + ":" + time[MINUTE];
    }

    public String getTimeEnd() {
        String[] time = this.timeEnd.split(":");
        return time[HOUR] + ":" + time[MINUTE];
    }

    public String getSubject() {
        return subject;
    }

    public LessonType getLessonType() {
        return lessonType;
    }

    public LectorLesson getLector() {
        return lector;
    }

    public String getLocation() {
        return location;
    }

    public GroupLesson getGroup() {
        return group;
    }

    public ArrayList<LectorLocation> getLectorLocations() {
        return lectorLocations;
    }
}
