package app.gdee.vekstud.VekAPI.JSON;

/**
 * Created by infraymer on 17.03.17.
 */

public class JsonRequest {

    /***********************************************************************************************
     * Пользователь
     **********************************************************************************************/

    // Авторизация
    private static final String REQUEST_LOGIN =
            "{\"phone\"     :       \"%1$s\", " +
            "\"password\"   :       \"%2$s\"}";

    public static String login(String phone, String password) {
        return String.format(REQUEST_LOGIN, phone, password);
    }

    // Регистрация
    private static final String REQUEST_SIGNUP =
            "{\"fname\"         :   \"%1$s\", " +
            "\"lname\"          :   \"%2$s\", " +
            "\"phone\"          :   \"%3$s\", " +
            "\"password\"       :   \"%4$s\", " +
            "\"confirmPassword\":   \"%5$s\"}";

    public static String signup(String firstName, String lastName, String phone, String password, String confirmPassword) {
        return String.format(REQUEST_SIGNUP, firstName, lastName, phone, password, confirmPassword);
    }

    /**
     * Подтверждение регистрации
     */

    // Смс с кодом подтверждения регистрации
    private static final String REQUEST_CONFIRM_SEND_SMS =
            "{\"phone\" : \"%1$s\"}";

    public static String confirmSendSms(String phone) {
        return String.format(REQUEST_CONFIRM_SEND_SMS, phone);
    }

    // Подтверждение номера телефона
    private static final String REQUEST_CONFIRM_CHECK_SMS =
            "{\"phone\" :   \"%1$s\", " +
            "\"code\"   :   \"%2$s\"}";

    public static String getRequestConfirmCheckSms(String phone, String code) {
        return String.format(REQUEST_CONFIRM_CHECK_SMS, phone, code);
    }

    /**
     * Восстановление пароля
     */

    // Смс с кодом подтверждения восстановления пароля
    private static final String REQUEST_RESET_SEND_SMS =
            "{\"phone\" : \"%1$s\"}";

    public static String resetSendSms(String phone) {
        return String.format(REQUEST_RESET_SEND_SMS, phone);
    }

    // Восстановления пароля
    private static final String REQUEST_RESET =
            "{\"phone\"             : \"%1$s\", " +
            "\"code\"               : \"%2$s\", " +
            "\"newPassword\"        : \"%3$s\", " +
            "\"newPasswordConfirm\" : \"%4$s\"}";

    public static String reset(String phone, String code, String newPassword, String newPasswordConfirm) {
        return String.format(REQUEST_RESET, phone, code, newPassword, newPasswordConfirm);
    }

    /***********************************************************************************************
     * Выбор группы
     **********************************************************************************************/

    // Выбор группы
    private static final String REQUEST_JOIN_GROUP =
            "{\"groupId\" : \"%1$s\"}";

    public static String selectGroup(int groupId) {
        return String.format(REQUEST_JOIN_GROUP, groupId);
    }

}
