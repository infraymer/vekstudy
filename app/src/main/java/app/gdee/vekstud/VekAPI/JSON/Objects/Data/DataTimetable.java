package app.gdee.vekstud.VekAPI.JSON.Objects.Data;

import android.support.v4.util.ArrayMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.gdee.vekstud.VekAPI.JSON.Objects.Lesson.Lesson;

/**
 * Created by infraymer on 07.04.17.
 */

public class DataTimetable {

    @SerializedName("data")
    @Expose
    private ArrayMap<String, ArrayList<Lesson>> timetable;

    public void setTimetable(ArrayMap<String, ArrayList<Lesson>> timetable) {
        this.timetable = timetable;
    }

    public ArrayMap<String, ArrayList<Lesson>> getTimetable() {
        return timetable;
    }
}
