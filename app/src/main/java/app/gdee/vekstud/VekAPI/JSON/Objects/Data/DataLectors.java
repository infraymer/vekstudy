package app.gdee.vekstud.VekAPI.JSON.Objects.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.gdee.vekstud.VekAPI.JSON.Objects.Lector;

/**
 * Created by infraymer on 07.04.17.
 */

public class DataLectors {

    @SerializedName("data")
    @Expose
    private ArrayList<Lector> lectors;

    public ArrayList<Lector> getLectors() {
        return lectors;
    }
}
