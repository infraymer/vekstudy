package app.gdee.vekstud.VekAPI

import app.gdee.vekstud.Extensions.enqueue
import app.gdee.vekstud.Helpers.User
import app.gdee.vekstud.Helpers.Utils
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable
import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.VekAPI.JSON.Objects.Group
import app.gdee.vekstud.VekAPI.JSON.Objects.Lector
import app.gdee.vekstud.VekAPI.JSON.Objects.University
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Created by infraymer on 17.03.17.
 */

object VekProvider {

    private const val BASE_URL = "https://schedule.vekclub.com/api/v1/"
    private const val BASE_URL_TEST = "https://test-schedule.vekclub.com/api/v1/"
    val vekAPI: VekAPI

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        this.vekAPI = retrofit.create(VekAPI::class.java)
    }

    /***********************************************************************************************
     * Public funs
     **********************************************************************************************/
    fun getUniversities(res: (universities: ArrayList<University>?) -> Unit) {
        vekAPI.universities.enqueue(
                { res(it.body()?.universities as ArrayList<University>) },
                { res(null) }
        )
    }

    fun getEntity(res: (entity: ArrayList<Entity>?) -> Unit) {
        doAsync {
            var list = arrayListOf<Entity>()
            val groups = vekAPI.getGroups(User.univerID).execute().body()
            if (groups != null)
                list.addAll(groups.groups)
            val lectors = vekAPI.getLectors(User.univerID).execute().body()
            if (lectors != null)
                list.addAll(lectors.lectors)
            uiThread {
                if (groups == null && lectors == null)
                    res(null)
                else
                    res(list)
            }
        }
    }

    fun getGroups(res: (groups: ArrayList<Group>) -> Unit) {
        fun req() {
            vekAPI.getGroups(User.univerID).enqueue(
                    { res(it.body()?.groups as ArrayList<Group>) },
                    { /*req()*/ }
            )
        }
    }

    fun getLectors(res: (groups: ArrayList<Lector>) -> Unit) {
        fun req() {
            vekAPI.getLectors(User.univerID).enqueue(
                    { res(it.body()?.lectors as ArrayList<Lector>) },
                    { /*req()*/ }
            )
        }
    }

    fun getTimetable(entityID: Int,
                     entityGroup: Boolean,
                     success: (response: DataTimetable) -> Unit,
                     failure: (t: Throwable) -> Unit) {
        if (entityGroup)
            vekAPI.getTimetable(entityID).enqueue(
                    { response ->
                        var dataTimetable = Utils.searchAndRemovingDuplicates(response.body())
                        success(dataTimetable)
                    },
                    { t ->
                        failure(t)
                    }
            )
        else
            vekAPI.getTimetableLectors(entityID).enqueue(
                    { response ->
                        if (response.body() != null)
                            success(response.body()!!)
                    },
                    { t ->
                        failure(t)
                    }
            )
    }
}
