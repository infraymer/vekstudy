package app.gdee.vekstud.VekAPI.JSON.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by infraymer on 23.04.17.
 */

public class PrimitiveObject {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isEqual(PrimitiveObject object) {
        if (this.getId() == object.getId() && this.getName().equals(object.getName()))
            return true;
        else
            return false;
    }
}
