package app.gdee.vekstud.VekAPI.JSON.Objects.Lesson

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by infraymer on 26.04.17.
 */

class LectorLesson {

    @SerializedName("lector_id")
    @Expose
    val id: Int = 0

    @SerializedName("lector_name")
    @Expose
    val name: String? = null
}
