package app.gdee.vekstud.VekAPI;

import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataGroups;
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataLectors;
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataUniversities;
import app.gdee.vekstud.VekAPI.JSON.Objects.VekResponse;
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by G_Dee on 20.02.2017.
 */

public interface VekAPI {

    // СПИСОК УНИВЕРСИТЕТОВ
    // @Headers("authorization: Bearer " + Const.TOKEN)
    @GET("handbook/institutions")
    Call<DataUniversities> getUniversities();

    // СПИСОК ГРУПП
    //@Headers("authorization: Bearer " + Const.TOKEN)
    @GET("handbook/groups-by-institution")
    Call<DataGroups> getGroups(@Query("institutionId") int universityId);

    // СПИСОК ПРЕПОДОВ
    //@Headers("authorization: Bearer " + Const.TOKEN)
    @GET("handbook/lecturers-in-institution")
    Call<DataLectors> getLectors(@Query("institutionId") int universityId);

    // ВЫБОР ГРУППЫ
    @Headers({
            "Content-Type: application/json;charset=UTF-8"
            //"authorization: Bearer " + Const.TOKEN
    })
    @POST("user/join-group")
    Call<VekResponse> selectGroup(@Body String body);

    // РАПИСАНИЕ ПО ГРУППЕ
    // @Headers("authorization: Bearer " + Const.TOKEN)
    @GET("schedule/group")
    Call<DataTimetable> getTimetable(@Query("groupId") int groupId);

    // РАПИСАНИЕ ПО ГРУППЕ
    // @Headers("authorization: Bearer " + Const.TOKEN)
    @GET("schedule/lector")
    Call<DataTimetable> getTimetableLectors(@Query("lectorId") int lectorId);

}
