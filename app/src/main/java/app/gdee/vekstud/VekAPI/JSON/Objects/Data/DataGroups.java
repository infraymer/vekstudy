package app.gdee.vekstud.VekAPI.JSON.Objects.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.gdee.vekstud.VekAPI.JSON.Objects.Group;

/**
 * Created by infraymer on 07.04.17.
 */

public class DataGroups {

    @SerializedName("data")
    @Expose
    private ArrayList<Group> groups;

    public ArrayList<Group> getGroups() {
        return groups;
    }
}
