package app.gdee.vekstud.VekAPI.JSON.Objects.Lesson

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by infraymer on 13.04.17.
 */

class SubgroupLesson {

    @SerializedName("subgroup_id")
    @Expose
    val id: Int = 0

    @SerializedName("subgroup_name")
    @Expose
    val name: String? = null
}
