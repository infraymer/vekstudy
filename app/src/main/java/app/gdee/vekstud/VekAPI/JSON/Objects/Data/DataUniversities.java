package app.gdee.vekstud.VekAPI.JSON.Objects.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.gdee.vekstud.VekAPI.JSON.Objects.University;

/**
 * Created by infraymer on 07.04.17.
 */

public class DataUniversities {

    @SerializedName("data")
    @Expose
    private ArrayList<University> universities;

    public ArrayList<University> getUniversities() {
        return universities;
    }
}
