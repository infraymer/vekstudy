package app.gdee.vekstud.VekAPI.JSON.Objects

import android.os.Parcel
import android.os.Parcelable
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion

/**
 * Created by infraymer on 23.04.17.
 */

open class Entity() : PrimitiveObject(), SearchSuggestion {
    val uuid = "${id}_$name"

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun getBody(): String {
        return name
    }

    companion object CREATOR : Parcelable.Creator<Entity> {
        override fun createFromParcel(parcel: Parcel): Entity {
            return Entity(parcel)
        }

        override fun newArray(size: Int): Array<Entity?> {
            return arrayOfNulls(size)
        }
    }
}
