package app.gdee.vekstud.Helpers;

import android.graphics.Color;
import android.support.v4.util.ArrayMap;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import app.gdee.vekstud.Adapters.LectorLocation;
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable;
import app.gdee.vekstud.VekAPI.JSON.Objects.Lesson.Lesson;

/**
 * Created by Vitaly on 20.08.2015.
 */
public class Utils {

    // Возвращает дату в формате гггг-ММ-дд
    public static String getDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    // Возвращает дату в формате дд.MM.гггг
    public static String getDateInvers(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

    // Возвращает дату в формате дд.MM
    public static String getDateMini(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM");
        return dateFormat.format(date);
    }

    // Возвращает время в формате чч:мм
    public static String getTime(long time) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        return timeFormat.format(time);
    }

    // Возвращает дату и время в формате гггг-ММ-дд ЧЧ:мм
    public static String getFullDate(long date) {
        SimpleDateFormat fullDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return fullDateFormat.format(date);
    }

    // Возвращает дату в милисекундах из строки в формате гггг-ММ-дд
    public static long getDate(String date) {
        String[] s = date.split("-");

        int year = Integer.parseInt(s[0]);
        int month = Integer.parseInt(s[1]);
        int day = Integer.parseInt(s[2]);

        final Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTimeInMillis();
    }

    // Возвращает время в милисекундах из строки в формате ЧЧ:мм
    public static long getTime(String time) {
        String[] s = time.split(":");

        int hourOfDay = Integer.parseInt(s[0]);
        int minute = Integer.parseInt(s[1]);

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTimeInMillis();
    }

    // Возвращает наименование дня недели
    public static String getDayOfWeekName(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case 2:
                return "Понедельник";
            case 3:
                return "Вторник";
            case 4:
                return "Среда";
            case 5:
                return "Четверг";
            case 6:
                return "Пятница";
            case 7:
                return "Суббота";
            case 1:
                return "Воскресенье";
            default:
                return "";
        }
    }

    // Возвращает наименование дня недели
    public static String getDayOfWeekName(String date) {
        return getDayOfWeekName(getDate(date));
    }

    // Возвращает наименование месяца
    public static String getMonthName(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int month = calendar.get(Calendar.MONTH);
        switch (month) {
            case 0:
                return "Января";
            case 1:
                return "Февраля";
            case 2:
                return "Марта";
            case 3:
                return "Апреля";
            case 4:
                return "Мая";
            case 5:
                return "Июня";
            case 6:
                return "Июля";
            case 7:
                return "Августа";
            case 8:
                return "Сентября";
            case 9:
                return "Октября";
            case 10:
                return "Ноября";
            case 11:
                return "Декабря";
            default:
                return "";
        }
    }

    // Возвращает наименование месяца
    public static String getMonthName(String date) {
        return getMonthName(getDate(date));
    }

    // Возваращает день месяца
    public static String getDayOfMonth(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    // Возваращает день месяца
    public static String getDayOfMonth(String date) {
        return getDayOfMonth(getDate(date));
    }

    // Поиск и удаление дубликатов в расписании
    public static DataTimetable searchAndRemovingDuplicates(DataTimetable dataTimetable) {
        ArrayMap<String, ArrayList<Lesson>> map = dataTimetable.getTimetable();

        for (int i = 0; i < map.size(); i++) {
            ArrayList<Lesson> lessons = map.valueAt(i);

            for (Lesson lesson : lessons) {
                lesson.fillLectorLocations();
            }

            for (int j = 1; j < lessons.size(); j++) {
                String currentTimeBegin = lessons.get(j).getTimeBegin();
                String currentSubject = lessons.get(j).getSubject();
                // String currentSubgroup = mLessons.get(i).getGroup().getSubgroup().getName();
                String previousTimeBegin = lessons.get(j - 1).getTimeBegin();
                String previousSubject = lessons.get(j - 1).getSubject();
                //String previousSubgroup = mLessons.get(i - 1).getGroup().getSubgroup().getName();

                if (currentSubject.equals(previousSubject)
                        && currentTimeBegin.equals(previousTimeBegin)) {
                    lessons.get(j - 1).addLectorLocation(
                            new LectorLocation(lessons.get(j).getLector().getName(),
                                    lessons.get(j).getLocation()
                            )
                    );
                    lessons.remove(j);
                    j--;
                }
            }
        }

        dataTimetable.setTimetable(map);
        return dataTimetable;
    }

    public static int getColorType(int typeId) {
        switch (typeId) {
            case 1:
                return Color.parseColor("#4e95f4");
            case 2:
                return Color.parseColor("#2da9bf");
            case 3:
                return Color.parseColor("#77cb1c");
            case 4:
                return Color.parseColor("#815bde");
            case 5:
                return Color.parseColor("#ee4364");
            case 6:
                return Color.parseColor("#fb9500");
            default:
                return Color.parseColor("#915437");
        }
    }
}
