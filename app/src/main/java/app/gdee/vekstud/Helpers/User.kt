package app.gdee.vekstud.Helpers

import com.chibatching.kotpref.KotprefModel


/**
 * Created by infraymer on 27.05.17.
 */
object User : KotprefModel() {
    var univerID by intPref(-1)
    var univerName by stringPref()

    var entityPK by stringPref()
    var entityID by intPref(-1)
    var entityName by stringPref()
    var isGroup by booleanPref(true)


}