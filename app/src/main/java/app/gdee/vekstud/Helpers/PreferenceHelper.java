package app.gdee.vekstud.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by G_Dee on 20.01.2017.
 */

public class PreferenceHelper {

    public static final String UNIVERSITY_ID = "univer_id";
    public static final String UNIVERSITY_NAME = "univer_name";
    public static final String ROLE_ID = "role_id";
    public static final String ROLE_NAME = "role_name";
    public static final String ROLE_IS_STUDENT = "role_student";


    private static PreferenceHelper instance;

    private SharedPreferences preferences;

    private PreferenceHelper(Context context) {
        preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }

    public static PreferenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceHelper(context);
        }
        return instance;
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public int getInt(String key) {
        return preferences.getInt(key, -1);
    }

    public void reset() {
        preferences.edit().clear().apply();
    }
}
