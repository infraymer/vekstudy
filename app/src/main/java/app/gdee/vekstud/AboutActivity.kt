package app.gdee.vekstud

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.LinkMovementMethod
import android.view.View
import app.gdee.vekstud.Helpers.Utils
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        initToolBar()

        val version = BuildConfig.VERSION_NAME
        val data = Utils.getDateInvers(BuildConfig.buildTime)

        this.tv_about_version.text = "Версия $version от $data"

        this.tv_about_link.movementMethod = LinkMovementMethod.getInstance()
        this.tv_about_link.visibility = View.INVISIBLE

        this.tv_about_other_app.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initToolBar() {
        this.about_toolbar.title = getString(R.string.drawer_item_about)
        this.about_toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_back_vector)
        this.about_toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        finish()
    }
}
