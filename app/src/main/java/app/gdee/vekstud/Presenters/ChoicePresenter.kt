package app.gdee.vekstud.Presenters

import app.gdee.vekstud.Helpers.User
import app.gdee.vekstud.R
import app.gdee.vekstud.ScheduleManager
import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.VekAPI.JSON.Objects.Group
import app.gdee.vekstud.VekAPI.JSON.Objects.University
import app.gdee.vekstud.VekAPI.VekProvider
import app.gdee.vekstud.Views.ChoiceView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

/**
 * Created by infraymer on 03.09.17.
 */
@InjectViewState
class ChoicePresenter : MvpPresenter<ChoiceView>() {

    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    private var mUniversityList = arrayListOf<University>()
    private var mEntityList = arrayListOf<Entity>()

    private var curUniverID     = User.univerID
    private var curUniverName   = User.univerName
    private var curEntityPK     = User.entityPK
    private var curEntityID     = User.entityID
    private var curEntityName   = User.entityName
    private var curIsGroup   = User.isGroup

    /***********************************************************************************************
     * Init
     **********************************************************************************************/
    init {

    }

    override fun attachView(view: ChoiceView?) {
        super.attachView(view)
        getUniversities()
    }

    /***********************************************************************************************
     * Private methods
     **********************************************************************************************/
    private fun getUniversities() {
        viewState.showProgress()
        VekProvider.getUniversities {
            viewState.hideProgress()
            if (it != null) {
                viewState.showListUniversity(it)
            } else {
                viewState.showError(R.string.error_text)
            }
        }
    }

    private fun getEntity() {
        VekProvider.getEntity {
            if (it != null) {
                mEntityList = it
                viewState.setSearchEntityList(mEntityList)
            }
            else {
                viewState.showMessage(R.string.choice_lost_connection)
            }
            viewState.hideSearchProgress()
        }
    }

    /***********************************************************************************************
     * For View
     **********************************************************************************************/
    fun onHomeActionSearchClicked() {
        viewState.hideSearch()
    }

    fun onQueryChanged(text: String) {
        val list = mEntityList.filter { it.name.contains(text) }
        viewState.setSearchEntityList(list as ArrayList<Entity>)
    }

    fun onUpdateButtonClicked() {
        viewState.hideError()
        getUniversities()
    }

    fun onUniverClicked(university: University) {
        viewState.showSearch()
        viewState.showSearchProgress()
        getEntity()
        User.univerID = university.id
        User.univerName = university.name
    }

    fun onSuggestionItemClicked(body: String) {
        try {
            val ent = mEntityList.first { it.name.contains(body) }
            User.entityPK = ent.uuid
            User.entityID = ent.id
            User.entityName = ent.name
            User.isGroup = ent is Group
            viewState.finishActivity()
            ScheduleManager.mChoiceFinishCallback.onFinish(true)
        } catch (e: Exception) { }
    }

    fun onBackPressed() {
        User.univerID = curUniverID
        User.univerName = curUniverName
        User.entityPK = curEntityPK
        User.entityID = curEntityID
        User.entityName = curEntityName
        User.isGroup = curIsGroup
        viewState.finishActivity()
        ScheduleManager.mChoiceFinishCallback.onFinish(false)
    }
}