package app.gdee.vekstud.Presenters

import app.gdee.vekstud.Views.SearchView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

/**
 * Created by infraymer on 01.09.17.
 */
@InjectViewState
class SearchPresenter : MvpPresenter<SearchView>() {

    fun onSearchButtonClicked() {

    }

    fun onHomeButtonClicked() {

    }

    fun onSearchItemClicked() {

    }
}