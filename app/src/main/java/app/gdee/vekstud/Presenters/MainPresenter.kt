package app.gdee.vekstud.Presenters

import app.gdee.vekstud.Extensions.convertToRealmLessons
import app.gdee.vekstud.Helpers.User
import app.gdee.vekstud.Realm.EntityR
import app.gdee.vekstud.ScheduleManager
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable
import app.gdee.vekstud.VekAPI.VekProvider
import app.gdee.vekstud.Views.MainView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.realm.Realm

/**
 * Created by infraymer on 17.05.17.
 */
@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    private val mRealm = Realm.getDefaultInstance()
    private var mEntity: EntityR? = null

    /***********************************************************************************************
     * Init
     **********************************************************************************************/
    init {
        mEntity = getEntityFromRealm()
        fun updateSchedule() {
            mEntity = getEntityFromRealm()
            if (mEntity == null) {
                mEntity = EntityR()
                mEntity!!.PK = User.entityPK
                mEntity!!.id = User.entityID
                mEntity!!.name = User.entityName
                mEntity!!.isGroup = User.isGroup
                viewState.setTitle(mEntity!!.name)
                downloadTimetable()
            } else {
                viewState.updateTimetable()
            }
        }

        if (User.entityPK.isEmpty()) {
            viewState.startChoiceActivity()
        } else {
            updateSchedule()
        }
        ScheduleManager.setChoiceFinishCallback {
            if (it)
                updateSchedule()
        }
    }

    /***********************************************************************************************
     * Private funs
     **********************************************************************************************/
    private fun putToRealm(dataTimetable: DataTimetable) {
        val lessons = dataTimetable.convertToRealmLessons()
        mEntity!!.lessons = lessons
        mRealm.executeTransaction { it.copyToRealmOrUpdate(mEntity) }
    }

    private fun downloadTimetable() {
        viewState.showProgress()
        VekProvider.getTimetable(User.entityID, User.isGroup,
                {
                    putToRealm(it)
                    viewState.updateTimetable()
                    viewState.hideProgress()
                },
                {
                    viewState.hideProgress()
                    viewState.showError()
                })
    }

    private fun getEntityFromRealm(): EntityR? {
        var ent: EntityR? = null
        try {
            ent = mRealm.where(EntityR::class.java).equalTo("PK", User.entityPK).findFirst()
        } catch (e: Exception) { }
        return ent
    }

    /***********************************************************************************************
     * Properties
     **********************************************************************************************/
    fun onUpdateButtonClicked() {
        downloadTimetable()
    }

    fun onCreated() {
    }
}
