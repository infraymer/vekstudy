package app.gdee.vekstud.Presenters

import app.gdee.vekstud.R
import app.gdee.vekstud.Views.DrawerView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

/**
 * Created by infraymer on 01.09.17.
 */
@InjectViewState
class DrawerPresenter : MvpPresenter<DrawerView>() {

    fun onMenuItemClicked(id: Int) {
        when (id) {
            R.id.nav_timetable -> true
            R.id.nav_share -> viewState.startShareVia()
            R.id.nav_settings -> viewState.startChoiceEntity()
            R.id.nav_about -> viewState.startAboutApp()
        }
    }
}