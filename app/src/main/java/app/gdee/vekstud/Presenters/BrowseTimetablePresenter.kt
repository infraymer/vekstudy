/*
package app.gdee.vekstud.Presenters

import app.gdee.vekstud.App
import app.gdee.vekstud.Helpers.UserInfo
import app.gdee.vekstud.Model
import app.gdee.vekstud.Presenters.Interfaces.IPresenterSearch
import app.gdee.vekstud.Presenters.Interfaces.IPresenterTimetable
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable
import app.gdee.vekstud.VekAPI.JSON.Objects.Group
import app.gdee.vekstud.VekAPI.JSON.Objects.PrimitiveObject

*/
/**
 * Created by infraymer on 18.05.17.
 *//*


class BrowseTimetablePresenter(internal var mViewTimetable: IViewTimetable,
                               internal var mViewSearch: IViewSearch) :
        IPresenterTimetable, IPresenterSearch, Model.OnDataListener {


    private var mModel = Model(this)

    private val mResources = App.getContext().resources

    private var mRoleId: Int = 0
    private var mRoleName: String = ""
    private var mIsStudent: Boolean = false


    fun onStart(id: Int, name: String, isStudent: Boolean) {
        mRoleId = id
        mRoleName = name
        mIsStudent = isStudent
        mViewTimetable.setTittle(name, isStudent)
        mViewTimetable.showProgress()
        mModel.getTimetable(id, isStudent)
    }


    override fun onStart() {
        mViewTimetable.setTittle(mRoleName, mIsStudent)
        mViewTimetable.showProgress()
        mModel.getTimetable(mRoleId, mIsStudent)
    }

    override fun onStop() {
        mModel.clearCacheRole()
    }

    override fun onUpdateButtonClicked() {
        mViewTimetable.hideError()
        onStart()
    }

    override fun onListItemClicked(primitiveObject: PrimitiveObject) {
        var isStudent = false
        if (primitiveObject is Group) {
            isStudent = true
        }
        mViewTimetable.startBrowseTimetableView(primitiveObject.id, primitiveObject.name, isStudent)
    }

    override fun onSearchViewOpened() {
        val univerID = User.univerID
        mModel.getAllRoles(univerID)
        mViewSearch.showSearch()
    }

    override fun onSearchViewClosed() {
        mViewSearch.hideSearch()
    }

    override fun responseUniversities(universities: List<*>?) {

    }

    override fun responseLectors(lectors: List<*>?) {

    }

    override fun responseGroups(groups: List<*>?) {

    }

    override fun responseTimetable(dataTimetable: DataTimetable?, isStudent: Boolean) {
        mViewTimetable.hideProgress()
        if (dataTimetable != null) {
            mModel.putTimetableToCacheRole(mRoleId, mRoleName, isStudent, dataTimetable)
            mViewTimetable.showTimetable(mRoleId, mRoleName, isStudent)
        } else {
            mViewTimetable.showError()
        }
    }

    override fun responseAllRoles(roles: List<*>) {
        mViewSearch.showDataSearchList(roles)
    }
}
*/
