/*
package app.gdee.vekstud

import app.gdee.vekstud.Helpers.UserInfo
import app.gdee.vekstud.Helpers.Utils
import app.gdee.vekstud.Models.IVekModel
import app.gdee.vekstud.Realm.*
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataGroups
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataLectors
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataTimetable
import app.gdee.vekstud.VekAPI.JSON.Objects.Data.DataUniversities
import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.VekAPI.JSON.Objects.Group
import app.gdee.vekstud.VekAPI.JSON.Objects.Lector
import app.gdee.vekstud.VekAPI.JSON.Objects.University
import app.gdee.vekstud.VekAPI.VekProvider
import io.realm.Realm
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.doAsyncResult
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

*/
/**
 * Created by infraymer on 13.05.17.
 *//*


class Model(private var mDataListener: OnDataListener) : IVekModel {

    interface OnDataListener {
        fun responseUniversities(universities: List<*>?)

        fun responseLectors(lectors: List<*>?)

        fun responseGroups(groups: List<*>?)

        fun responseTimetable(dataTimetable: DataTimetable?, isStudent: Boolean)

        fun responseAllRoles(roles: List<*>)
    }

    private val mRealm = Realm.getDefaultInstance()


    fun putTimetableToFavRole(roleId: Int, roleName: String, isStudent: Boolean, dataTimetable: DataTimetable?) {
        mRealm.executeTransaction { it ->
            if (dataTimetable != null) {
                val roleR = it.createObject(EntityR::class.java)
                roleR.id = roleId
                roleR.name = roleName
                roleR.isGroup = isStudent

                for (list in dataTimetable.timetable) {
                    for (item in list.value) {
                        // Создаем занятите и кладем примитивчики
                        val lesson = it.createObject(LessonR::class.java)
                        lesson.date = item.date
                        lesson.timeBegin = item.timeBegin
                        lesson.timeEnd = item.timeEnd
                        lesson.subjectName = item.subject
                        lesson.location = item.location

                        // Кладем группу
                        if (item.group != null) {
                            lesson.groupId = item.group.id
                            lesson.groupName = item.group.name!!
                            if (item.group.subgroup != null) {
                                lesson.subgroupId = item.group.subgroup!!.id
                                lesson.subgroupName = item.group.subgroup!!.name!!
                            }
                        }

                        // Кладем препода
                        if (item.lector != null) {
                            lesson.lectorId = item.lector.id
                            lesson.lectorName = item.lector.name!!
                        }

                        // Кладем тип
                        lesson.typeId = item.lessonType.id
                        lesson.typeName = item.lessonType.name

                        // Кладем список групп
                        if (item.groups != null) {
                            for (group in item.groups) {
                                val g = it.createObject(GroupR::class.java)
                                g.id = group.id
                                g.name = group.name!!
                                if (group.subgroup != null)
                                    g.subgroup = group.subgroup!!.name!!
                                lesson.groups.add(g)
                            }
                        }

                        // Кладем сочетание препод-аудитория
                        if (item.lectorLocations != null) {
                            for (lecLoc in item.lectorLocations) {
                                val ll = it.createObject(LectorLocationR::class.java)
                                ll.lector = lecLoc.lector
                                ll.location = lecLoc.location
                                lesson.lectorLocations.add(ll)
                            }
                        }

                        roleR.lessons.add(lesson)
                    }
                }

            }
        }
    }

    fun putTimetableToCacheRole(roleId: Int, roleName: String, isStudent: Boolean, dataTimetable: DataTimetable?) {
        mRealm.executeTransaction { it ->
            if (dataTimetable != null) {
                val roleR = it.createObject(CacheRoleR::class.java)
                roleR.id = roleId
                roleR.name = roleName
                roleR.isStudent = isStudent

                for (list in dataTimetable.timetable) {
                    for (item in list.value) {
                        // Создаем занятите и кладем примитивчики
                        val lesson = it.createObject(LessonR::class.java)
                        lesson.date = item.date
                        lesson.timeBegin = item.timeBegin
                        lesson.timeEnd = item.timeEnd
                        lesson.subjectName = item.subject
                        lesson.location = item.location

                        // Кладем группу
                        if (item.group != null) {
                            lesson.groupId = item.group.id
                            lesson.groupName = item.group.name!!
                            if (item.group.subgroup != null) {
                                lesson.subgroupId = item.group.subgroup!!.id
                                lesson.subgroupName = item.group.subgroup!!.name!!
                            }
                        }

                        // Кладем препода
                        if (item.lector != null) {
                            lesson.lectorId = item.lector.id
                            lesson.lectorName = item.lector.name!!
                        }

                        // Кладем тип
                        lesson.typeId = item.lessonType.id
                        lesson.typeName = item.lessonType.name

                        // Кладем список групп
                        if (item.groups != null) {
                            for (group in item.groups) {
                                val g = it.createObject(GroupR::class.java)
                                g.id = group.id
                                g.name = group.name!!
                                if (group.subgroup != null)
                                    g.subgroup = group.subgroup!!.name!!
                                lesson.groups.add(g)
                            }
                        }

                        // Кладем сочетание препод-аудитория
                        if (item.lectorLocations != null) {
                            for (lecLoc in item.lectorLocations) {
                                val ll = it.createObject(LectorLocationR::class.java)
                                ll.lector = lecLoc.lector
                                ll.location = lecLoc.location
                                lesson.lectorLocations.add(ll)
                            }
                        }

                        roleR.lessons.add(lesson)
                    }
                }

            }
        }
    }

    fun clearCacheRole() {
        doAsync {
            mRealm.executeTransaction { it ->
                it.where(CacheRoleR::class.java).findAll().deleteAllFromRealm()
            }
        }
    }

    fun clearDatabase() {
        doAsync {
            mRealm.executeTransaction { it ->
                it.where(EntityR::class.java).findAll().deleteAllFromRealm()
            }
        }
    }

    fun setUniversityToPref(university: University) {
        User.univerID = university.id
        User.univerName = university.name
    }

    fun setRoleToPref(entity: Entity) {
        User.entityID = entity.id
        User.entityName = entity.name
        when (entity) {
            is Group -> User.isGroup = true
            is Lector -> User.isGroup = false
        }
    }


    */
/***********************************************************************************************
     * IVekModel
     *//*

    override fun getUniversities() {
        VekProvider.vekAPI.universities.enqueue(object : Callback<DataUniversities> {
            override fun onResponse(call: Call<DataUniversities>, response: Response<DataUniversities>) {
                mDataListener.responseUniversities(response.body()!!.universities)
            }

            override fun onFailure(call: Call<DataUniversities>, t: Throwable) {
                mDataListener.responseUniversities(null)
            }
        })
    }

    override fun getGroups(groupId: Int) {
        VekProvider.vekAPI.getGroups(groupId).enqueue(object : Callback<DataGroups> {
            override fun onResponse(call: Call<DataGroups>, response: Response<DataGroups>) {
                if (response.body() != null)
                    mDataListener*/
/*.responseGroups(response.body().groups)*//*

                else
                    mDataListener.responseGroups(null)
            }

            override fun onFailure(call: Call<DataGroups>, t: Throwable) {
                mDataListener.responseGroups(null)
            }
        })
    }

    override fun getLectors(lectorId: Int) {
        VekProvider.vekAPI.getLectors(lectorId).enqueue(object : Callback<DataLectors> {
            override fun onResponse(call: Call<DataLectors>, response: Response<DataLectors>) {
                if (response.body() != null)
                    mDataListener*/
/*.responseLectors(response.body().lectors)*//*

                else
                    mDataListener.responseLectors(null)
            }

            override fun onFailure(call: Call<DataLectors>, t: Throwable) {
                mDataListener.responseLectors(null)
            }
        })
    }

    override fun getTimetable(roleId: Int, isStudent: Boolean) {
        if (isStudent)
            VekProvider.vekAPI
                    .getTimetable(roleId)
                    .enqueue(object : Callback<DataTimetable> {
                        override fun onResponse(call: Call<DataTimetable>, response: Response<DataTimetable>) {
                            var dataTimetable: DataTimetable? = response.body()
                            if (dataTimetable != null)
                                dataTimetable = Utils.searchAndRemovingDuplicates(dataTimetable)
                            mDataListener.responseTimetable(dataTimetable, isStudent)
                        }

                        override fun onFailure(call: Call<DataTimetable>, t: Throwable) {
                            mDataListener.responseTimetable(null, isStudent)
                        }
                    })
        else
            VekProvider.vekAPI
                    .getTimetableLectors(roleId)
                    .enqueue(object : Callback<DataTimetable> {
                        override fun onResponse(call: Call<DataTimetable>, response: Response<DataTimetable>) {
                            mDataListener.responseTimetable(response.body(), isStudent)
                        }

                        override fun onFailure(call: Call<DataTimetable>, t: Throwable) {
                            mDataListener.responseTimetable(null, isStudent)
                        }
                    })
    }

    fun getAllRoles(univerID: Int) {
        doAsyncResult {
            val groups = VekProvider.vekAPI
                    .getGroups(univerID)
                    .execute()
                    .body()?.groups

            val lectors = VekProvider.vekAPI
                    .getLectors(univerID)
                    .execute()
                    .body()?.lectors

            val list = mutableListOf<Entity>()

            if (groups != null) {
                list.addAll(groups)
            }
            if (lectors != null) {
                list.addAll(lectors)
            }

            uiThread { mDataListener.responseAllRoles(list) }
        }
    }
}
*/
