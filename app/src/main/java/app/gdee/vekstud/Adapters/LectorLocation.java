package app.gdee.vekstud.Adapters;

/**
 * Created by infraymer on 02.05.17.
 */

public class LectorLocation {

    private String lector;
    private String location;

    public LectorLocation(String lector, String location) {
        this.lector = lector;
        this.location = location;
    }

    public String getLector() {
        return lector;
    }

    public String getLocation() {
        return location;
    }
}
