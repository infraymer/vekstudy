package app.gdee.vekstud.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import app.gdee.vekstud.R;

/**
 * Created by infraymer on 27.04.17.
 */

public class LectorLocAdapter extends ArrayAdapter<LectorLocation> {

    public LectorLocAdapter(@NonNull Context context, @NonNull ArrayList<LectorLocation> list) {
        super(context, R.layout.lec_loc_list_item, list);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.lec_loc_list_item, null);
        }

        ((TextView) convertView.findViewById(R.id.text_lector_2))
                .setText(getItem(position).getLector());
        ((TextView) convertView.findViewById(R.id.text_location_2))
                .setText(getItem(position).getLocation());

        return convertView;
    }
}
