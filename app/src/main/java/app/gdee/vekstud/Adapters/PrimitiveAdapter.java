package app.gdee.vekstud.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.gdee.vekstud.R;
import app.gdee.vekstud.VekAPI.JSON.Objects.PrimitiveObject;

/**
 * Created by infraymer on 23.04.17.
 */

public class PrimitiveAdapter extends ArrayAdapter {

    private ArrayList mFilterList;
    private ArrayList mList = new ArrayList();

    public PrimitiveAdapter(@NonNull Context context, @NonNull List list) {
        super(context, R.layout.search_list_item_1, list);
        mList.addAll(list);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        PrimitiveObject role = (PrimitiveObject) getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.search_list_item_1, null);
        }
        ((TextView) convertView.findViewById(R.id.text1))
                .setText(role.getName());

        return convertView;
    }

    public PrimitiveObject getItem(int position) {
        return (PrimitiveObject) mList.get(position);
    }

    public void filter(String text) {
        mFilterList = new ArrayList();

        if (text.length() == 0) {
            mFilterList.addAll(mList);
        } else {
            for (Object item : mList) {
                PrimitiveObject object = (PrimitiveObject) item;
                if (object.getName().toLowerCase(Locale.getDefault()).contains(text)) {
                    mFilterList.add(item);
                }
            }
        }
        clear();
        addAll(mFilterList);
        notifyDataSetChanged();
    }
}
