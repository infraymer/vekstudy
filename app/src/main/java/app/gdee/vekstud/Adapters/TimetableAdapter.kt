package app.gdee.vekstud.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.gdee.vekstud.R
import app.gdee.vekstud.VekAPI.JSON.Objects.Lesson.Lesson
import kotlinx.android.synthetic.main.timetable_group_list_item.view.*
import kotlinx.android.synthetic.main.timetable_lector_list_item.view.*
import java.util.*

/**
 * Created by G_Dee on 19.01.2017.
 */

class TimetableAdapter(
        private val mLessons: ArrayList<Lesson>,
        private val mIsStudent: Boolean,
        private val mContext: Context) : RecyclerView.Adapter<TimetableAdapter.LessonViewHolder>() {


    class LessonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonViewHolder {
        val v: View
        when (mIsStudent) {
            true -> v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.timetable_group_list_item, parent, false)
            false -> v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.timetable_lector_list_item, parent, false)
        }

        return LessonViewHolder(v)
    }

    override fun onBindViewHolder(holder: LessonViewHolder, position: Int) {
        // Выбор отображения расписания
        when (mIsStudent) {
            true -> showLessonGroup(holder, position)
            false -> showLessonLector(holder, position)
        }
    }


    // Отображение расписания группы
    private fun showLessonGroup(holder: LessonViewHolder, position: Int) {
        // Отображение время начала занятия
        holder.itemView.group_timeBegin.text = mLessons[position].timeBegin
        // Отображение время конца занятия
        holder.itemView.group_timeEnd.text = mLessons[position].timeEnd
        // Отображение типа занятия
        holder.itemView.group_type.text = mLessons[position].lessonType.name.toLowerCase()
        holder.itemView.group_type.setBackgroundColor(mLessons[position].typeColor)
        // Инициализация адаптера отображения списка групп
        holder.itemView.group_subject.text = mLessons[position].subject

        // Здесь проскакивает баг, поэтому блок с отображением подгруппы засунул в try/catch
        try {
            if (mLessons[position].group.subgroup != null) {
                // Отображение подгруппы
                holder.itemView.group_subgroup.text = "Подгруппа " + mLessons[position].group.subgroup?.name
                holder.itemView.group_subgroup.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.message
        }

        // Блок с инициализацией адаптера для отображения списка пар "преподаватель-аудитория"
        // используется при ситуации с одним занятием сразу у нескольких преподавателей
        // например, Физическая культура в ЮГУ
        var lectorLocations: ArrayList<LectorLocation>? = mLessons[position].lectorLocations
        if (lectorLocations == null) {
            lectorLocations = ArrayList<LectorLocation>()
            lectorLocations.add(LectorLocation("Empty", "Empty"))
        }
        val adapter = LectorLocAdapter(mContext, lectorLocations)
        // Отображение списка "преподаватель-аудитория"
        holder.itemView.group_lecloc_list.adapter = adapter
    }

    // Отображение расписания препода
    private fun showLessonLector(holder: LessonViewHolder, position: Int) {
        // Отображение время начала занятия
        holder.itemView.lector_timeBegin.text = mLessons[position].timeBegin
        // Отображение время конца занятия
        holder.itemView.lector_timeEnd.text = mLessons[position].timeEnd
        // Отображение типа занятия
        holder.itemView.lector_type.text = mLessons[position].lessonType.name.toLowerCase()
        holder.itemView.lector_type.setBackgroundColor(mLessons[position].typeColor)
        // Отображение дисциплины
        holder.itemView.lector_subject.text = mLessons[position].subject
        // Инициализация адаптера отображения списка групп
        val adapter = GroupsAdapter(mContext, mLessons[position].namesGroups)
        // Отображение списка групп
        holder.itemView.lector_groups_list.adapter = adapter
        // Отображение аудитории занятия
        holder.itemView.lector_location.text = mLessons[position].location


        // Здесь проскакивает баг, поэтому блок с отображением подгруппы засунул в try/catch
        try {
            if (mLessons[position] != null) {
                // Отображение подгруппы
                holder.itemView.lector_subgroup.text = "Подгруппа " + mLessons[position].groups[0].subgroup?.name
                holder.itemView.lector_subgroup.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.message
        }

    }

    override fun getItemCount(): Int {
        return mLessons.size
    }
}
