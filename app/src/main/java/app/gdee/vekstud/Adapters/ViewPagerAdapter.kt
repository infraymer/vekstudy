package app.gdee.vekstud.Adapters

import android.content.Context
import android.graphics.Color
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.gdee.vekstud.Fragments.DayFragment
import app.gdee.vekstud.Helpers.Utils
import app.gdee.vekstud.R
import com.astuetz.PagerSlidingTabStrip
import java.util.*

/**
 * Created by infraymer on 17.05.17.
 */

class ViewPagerAdapter(fm: FragmentManager, val context: Context) :
        FragmentStatePagerAdapter(fm),
        PagerSlidingTabStrip.CustomTabProvider {

    init {
        notifyDataSetChanged()
    }


    override fun getItem(position: Int): Fragment {
        return DayFragment(position)
    }

    override fun getCount(): Int {
        return 14
    }


    /***********************************************************************************************
     * Custom tabs
     **********************************************************************************************/
    override fun getCustomTabView(parent: ViewGroup, position: Int): View {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, position)
        val date = Utils.getDate(calendar.timeInMillis)

        val v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null)
        val nameTab = v.findViewById(R.id.tab_name) as TextView
        val dateTab = v.findViewById(R.id.tab_date) as TextView
        nameTab.text = Utils.getDayOfWeekName(date)
        dateTab.text = Utils.getDateMini(calendar.timeInMillis)
        return v
    }

    override fun tabSelected(tab: View) {
        val nameTab = tab.findViewById(R.id.tab_name) as TextView
        val dateTab = tab.findViewById(R.id.tab_date) as TextView
        nameTab.setTextColor(Color.parseColor("#ffffff"))
        dateTab.setTextColor(Color.parseColor("#ffffff"))
    }

    override fun tabUnselected(tab: View) {
        val nameTab = tab.findViewById(R.id.tab_name) as TextView
        val dateTab = tab.findViewById(R.id.tab_date) as TextView
        nameTab.setTextColor(Color.parseColor("#cccccc"))
        dateTab.setTextColor(Color.parseColor("#cccccc"))
    }
}
