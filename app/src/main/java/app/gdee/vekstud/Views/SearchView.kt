package app.gdee.vekstud.Views

import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by infraymer on 01.09.17.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface SearchView : MvpView {

    fun showSearch()

    fun hideSearch()

    fun setSearchList()

    fun showSearchProgress()

    fun hideSearchProgress()

    fun setSearchEntityList(entity: ArrayList<Entity>)

}