package app.gdee.vekstud.Views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by infraymer on 01.09.17.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface DrawerView : MvpView {

    fun startShareVia()

    fun startChoiceEntity()

    fun startAboutApp()
}