package app.gdee.vekstud.Views

import app.gdee.vekstud.VekAPI.JSON.Objects.Entity
import app.gdee.vekstud.VekAPI.JSON.Objects.University
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by infraymer on 03.09.17.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface ChoiceView : MvpView {

    fun showMessage(resId: Int)

    fun showError(resId: Int)

    fun hideError()

    fun showListUniversity(universities: ArrayList<University>)

    fun setSearchEntityList(entity: ArrayList<Entity>)

    fun showProgress()

    fun hideProgress()

    fun setTitle(resId: Int)

    fun showSearch()

    fun hideSearch()

    fun showSearchProgress()

    fun hideSearchProgress()

    fun finishActivity()
}